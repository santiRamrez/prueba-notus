/* eslint-disable prettier/prettier */
import getData from './getData';
import SortedUsersArray from './SortedUsersArray'

let url = {
  users: 'http://localhost:3004/users',
  // positions: 'http://localhost:3004/positions',
  // contracts: 'http://localhost:3004/contracts',
  locations: 'http://localhost:3004/locations',
};

let SortedLocationsArray = async () => {
  let notSorted = [];
  let sorted = [];

  let UsersObj = await SortedUsersArray();
  let LocationsEndpoint = await getData(url.locations);

  LocationsEndpoint.forEach((location) => {
    let arrOfEmployees = []
    let obj = {}
    obj.id = location.id;
    obj.name = location.name;
    obj.address = `${location.address1}, ${location.address2 ? location.address2 + "," : " "} ${location.commune}, ${location.region}.`
    obj.schedule = `${location.startTime} - ${location.endTime}`

    UsersObj.forEach((user) => {
      if(user.locations === location.name) arrOfEmployees.push(location.name)
    })
    
    obj.quantityEmployees = arrOfEmployees.length

    notSorted.push(obj)
  })



  sorted = notSorted.sort((a, b) => {
    return b.name.toLowerCase().localeCompare(a.name.toLowerCase())
  })

  return sorted
}

export default SortedLocationsArray