/* eslint-disable prettier/prettier */
import getData from './getData';

let url = {
  users: 'http://localhost:3004/users',
  positions: 'http://localhost:3004/positions',
  contracts: 'http://localhost:3004/contracts',
  locations: 'http://localhost:3004/locations',
};

let SortedUsersArray = async () => {
  let notSorted = [];
  let sorted = [];

  let UsersEndpoint = await getData(url.users);
  let PositionsEndpoint = await getData(url.positions);
  let LocationsEndpoint = await getData(url.locations);
  let ContractsEndpoint = await getData(url.contracts);

  UsersEndpoint.forEach((user) => {
    let obj = {}
    
    //These 2 properties are taken from UsersEndpoint
    obj.employeeId = user.employeeId
    obj.fullName = `${user.firstName} ${user.lastName}`
    obj.id = user.id

    let matchPosition = [];
    //The if statement is hardcoded, i could not make it using a loop, of you know how to do it please tell me.
    PositionsEndpoint.forEach((position) => {
       if(position.id == user.positionId[0] || position.id == user.positionId[1]) matchPosition.push(position.name)
    })
    obj.positions = matchPosition.toString().replace(/,/, " - ");
    
    //The if statement is hardcoded, i could not make it using a loop, of you know how to do it please tell me.
    let matchLocation = [];
    LocationsEndpoint.forEach((location) => {
      if(location.id == user.locationId[0] || location.id == user.positionId[1]) matchLocation.push(location.name)
    })
    obj.locations = matchLocation.toString();


    let matchContract = ContractsEndpoint.filter((contract) => user.contract === contract.id )
    obj.contract = `${matchContract[0].legalMaxWeeklyHours} x semana`

    notSorted.push(obj)
  });

  //Sorted from lowest to highest employeeId
  sorted = notSorted.sort((a, b) => {
       if (a.employeeId < b.employeeId) return -1
       if (a.employeeId > b.employeeId) return 1
       if (a.employeeId == b.employeeId) return 0
    })
  return sorted;
};

export default SortedUsersArray
