/* eslint-disable prettier/prettier */
const getData = async (url) => {
  let apiURl = url;

  try {
    const response = await fetch(apiURl);
    const data = await response.json();
    //data is an array and have objects in.
    return data;
  } catch (error) {
    console.log("fetch error ", error);
  }
};

export default getData;
