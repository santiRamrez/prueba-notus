/* eslint-disable prettier/prettier */
import getData from "./getData";

let url = {
  turnTemplates: 'http://localhost:3004/turnTemplates',
  locations: 'http://localhost:3004/locations',
};

let ListTurnTemplates = async () => {
  let output = [];

  let LocationsArr = await getData(url.locations);
  let TurnTemplates = await getData(url.turnTemplates);

  TurnTemplates.forEach((template) => {
    let obj = {}
    obj.name = template.name;
    obj.checkIn = template.checkIn;
    obj.checkOut = template.checkOut;
    obj.breakTime = template.breakTime;

    let fromMinutesToHours = template.breakTime / 60
    let workHrs = (Number(template.checkOut.substr(0,2)) - fromMinutesToHours) - Number(template.checkIn.substr(0,2))
    obj.workHrs = workHrs

    obj.position = template.name.replace(/^(Apertura|Intermedio|Cierre)/, "");

    let matchLocation = []

    //The if statement is hardcoded, i could not make it using a loop, of you know how to do it please tell me.
    LocationsArr.forEach((location) => {
        if (!template.locationId || location.id == template.locationId[0] || location.id == template.locationId[1] || location.id == template.locationId[2]) {
          matchLocation.push(location.name)
        } 
    })
    
    obj.locations = matchLocation.join(" - ")

    output.push(obj)
  })

  return output;
}

export default ListTurnTemplates;