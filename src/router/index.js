import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import Calendar from '../views/Calendar.vue';
import Users from '../views/Users.vue';
import Locations from '../views/Locations.vue';
import TurnTemplates from '../views/TurnTemplates.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/calendario',
    name: 'Calendar',
    component: Calendar,
  },
  {
    path: '/usuarios',
    name: 'Users',
    component: Users,
  },
  {
    path: '/sucursales',
    name: 'sucursales',
    component: Locations,
  },
  {
    path: '/plantillas_turnos',
    name: 'TurnTemplates',
    component: TurnTemplates,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
